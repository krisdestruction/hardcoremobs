package info.terrismc.HardcoreMobs;

import org.bukkit.plugin.java.JavaPlugin;

public class HardcoreMobs extends JavaPlugin {
	private ConfigStore cStore;
	
	public void onEnable() {
		// Create storage objects
		cStore = new ConfigStore( this );
		
		// Create event listener
		this.getServer().getPluginManager().registerEvents( new EventListener( this ), this );
	}
	
	public ConfigStore getConfigStore() {
		return cStore;
	}
}
