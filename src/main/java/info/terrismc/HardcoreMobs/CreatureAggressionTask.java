package info.terrismc.HardcoreMobs;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class CreatureAggressionTask extends BukkitRunnable {
	private World world;
	private Creature creature;
	private int aggRadius;
	
	public CreatureAggressionTask ( HardcoreMobs plugin, Creature creature ) {
		this.creature = creature;
		this.world = creature.getWorld();
		this.aggRadius = aggRadius;
		this.runTaskTimer( plugin, 0, 1 * 20 );
		// TODO Add to task set
	}
	
	public void run() {
		// Check entity dead or world unloaded 
		if( creature.isDead() || world == null )
			this.cancel();
		
		// Check players
		List<Player> players = world.getPlayers();
		if( players.size() == 0 )
			return;
		
		// Select closest player 
		players.sort( new DistanceComparator( creature.getLocation() ) );
		Player player = players.get( 0 );
		
		// Set target if within aggRadius
		creature.setTarget( player );
		
		// Reset target when player dead
	}
	
}
