package info.terrismc.HardcoreMobs;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Bat;
import org.bukkit.entity.Blaze;
import org.bukkit.entity.CaveSpider;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Horse.Variant;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.MagmaCube;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Wolf;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

public class EventListener implements Listener {
	private HardcoreMobs plugin;
	
	public EventListener( HardcoreMobs plugin ) {
		this.plugin = plugin;
	}
	
	@EventHandler( priority = EventPriority.HIGH )
	public void onCreatureSpawn( CreatureSpawnEvent event ) {
		// Check cancelled
		if( event.isCancelled() )
			return;
		
		// Check world
		Entity entity = event.getEntity();
		World world = entity.getWorld();
		if( !plugin.getConfigStore().isEnabledWorld( world ) )
			return;
		
		// Check spawn reason for recursive call
		// Get entity location
		Location loc = entity.getLocation();
		if( entity.getType() == EntityType.BAT ) {
			// Make bats faster
			Bat bat = (Bat) entity;
			
			// Create potion effect
			//PotionEffect effect = new PotionEffect(null, Integer.MAX_VALUE, 2);
			//bat.addPotionEffect( effect );
			return;
		} else if( entity.getType() == EntityType.ZOMBIE ) {
			// Replace zombie with baby zombie
			
			// Set zombie attributes
			Zombie zombie = (Zombie) entity;
			zombie.setBaby( true );
			return;
		} else if( entity.getType() == EntityType.SKELETON ) {
			// Replace skeleton with flying skeleton
			
			// Spawn vehicle
			Bat bat = (Bat) world.spawnEntity( entity.getLocation(), EntityType.BAT );
			bat.setRemoveWhenFarAway( true );
			bat.setPassenger( entity );
			return;
		} else if( entity.getType() == EntityType.CREEPER ) {
			// Replace creeper with flying creeper
			
			// Spawn vehicle
			CaveSpider caveSpider = (CaveSpider) world.spawnEntity( loc, EntityType.CAVE_SPIDER );
			caveSpider.setPassenger( entity );
			
			// Set charged
			Creeper creeper = (Creeper) entity;
			creeper.setPowered( true );
			return;
		} else if( entity.getType() == EntityType.HORSE ) {
			// Replace horse with skeleton knight
			
			// Create horse
			Horse horse = (Horse) entity;
			horse.setAdult();
			horse.setVariant( Variant.SKELETON_HORSE );
			horse.setRemoveWhenFarAway( true );
			EntityEquipment horseEquipment = horse.getEquipment();
			horseEquipment.setBoots( new ItemStack( Material.IRON_BARDING ) );
			
			// Create skeleton
			Skeleton skeleton = (Skeleton) world.spawnEntity( loc, EntityType.SKELETON );
			skeleton.setRemoveWhenFarAway( false );
			EntityEquipment skeletonEquipment = skeleton.getEquipment();
			skeletonEquipment.setBoots( new ItemStack( Material.IRON_BOOTS ) );
			skeletonEquipment.setChestplate( new ItemStack( Material.IRON_CHESTPLATE ) );
			skeletonEquipment.setHelmet( new ItemStack( Material.IRON_HELMET ) );
			skeletonEquipment.setLeggings( new ItemStack( Material.IRON_LEGGINGS ) );
			horse.setPassenger( skeleton );
			return;
		} else if( entity.getType() == EntityType.SPIDER ) {
			// Replace spider with blaze jockey
			
			// Set passenger
			Blaze blaze = (Blaze) world.spawnEntity( loc, EntityType.BLAZE );
			entity.setPassenger( blaze );
			return;
		} else if( entity.getType() == EntityType.PIG ) {
			// Replace pig with pigzombie
			
			// Thunder location
			world.strikeLightning( loc );
			return;
		} else if( entity.getType() == EntityType.CHICKEN ) {
			// Replace chicken with chicken jockey
			
			// Spawn passenger
			Zombie zombie = (Zombie) world.spawnEntity( loc, EntityType.ZOMBIE );
			zombie.setBaby( true );
			entity.setPassenger( zombie );
			return;
		} else if( entity.getType() == EntityType.WOLF ) {
			Wolf wolf = (Wolf) entity;
			wolf.setAngry( true );
			return;
		} else if( entity.getType() == EntityType.IRON_GOLEM ) {
			IronGolem ironGolem = (IronGolem) entity;
			ironGolem.setPlayerCreated( false );
			return;
		} else if( entity.getType() == EntityType.ENDERMAN ) {
			// Set endermen aggressive
			Enderman enderman = (Enderman) entity;
			
			// Attack nearest target timer
			new CreatureAggressionTask( plugin, enderman );
			return;
		} else if( entity.getType() == EntityType.MAGMA_CUBE ) {
			// Replace magma cube with largest attackable magma cube
			MagmaCube magmaCube = (MagmaCube) entity;
			
			// Set attrbites
			magmaCube.setSize( 8 );
			return;
		}
		
		// Giant slime
		// Giant magma cube
		// Giant spawns horde
		// Giant has a lot more health
		// Bat has speed
	}
	
	@EventHandler
	public void onEntityDeath( EntityDeathEvent event ) {
		// Check world
		Entity entity = event.getEntity();
		World world = entity.getWorld();
		if( !plugin.getConfigStore().isEnabledWorld( world ) )
			return;
		
		// Kill vehicles
		Entity vehicle = entity.getVehicle();
		if( vehicle == null )
			return;
		
		// Kill bat
		if( vehicle.getType() == EntityType.BAT ) {
			vehicle.remove();
			return;
		}
		
		// Kill horse
		if( vehicle.getType() == EntityType.HORSE ) {
			Horse horse = (Horse) vehicle;
			Variant variant = horse.getVariant();
			if( variant == Variant.SKELETON_HORSE || variant == Variant.UNDEAD_HORSE )
				horse.damage( 9999 );
			return;
		}
	}
	
	@EventHandler
	public void onVehicleExit( VehicleExitEvent event ) {
		// Check cancelled
		if( event.isCancelled() )
			return;
		
		// Check world
		Entity entity = event.getExited();
		World world = entity.getWorld();
		if( !plugin.getConfigStore().isEnabledWorld( world ) )
			return;
		
		// Prevent undead and skeleton horse dismount
		Entity vehicle = event.getVehicle();
		if( vehicle.getType() == EntityType.HORSE ) {
			Horse horse = (Horse) vehicle;
			Variant variant = horse.getVariant();
			if( variant == Variant.SKELETON_HORSE || variant == Variant.UNDEAD_HORSE )
				event.setCancelled( true );
			return;
		}
	}
	
	@EventHandler
	public void onEntityTarget( EntityTargetEvent event ) {
		// Check cancelled
		if( event.isCancelled() )
			return;
		
		// Check world
		Entity entity = event.getEntity();
		World world = entity.getWorld();
		if( !plugin.getConfigStore().isEnabledWorld( world ) )
			return;
		
		// On target change, get vehicle to follow
	}
	
	@EventHandler
	public void onChunkLoad( final ChunkLoadEvent event ) {
		plugin.getServer().getScheduler().runTaskAsynchronously( plugin, new Runnable() {
			public void run() {
				// Check world
				if( !plugin.getConfigStore().isEnabledWorld( event.getWorld() ) )
					return;
				
				Entity[] entities = event.getChunk().getEntities();
				for( Entity entity : entities )
					if( entity.getType() == EntityType.ENDERMAN )
						new CreatureAggressionTask( plugin, (Enderman) entity );
			}
		});
	}
	
	@EventHandler
	public void onPlayerDeath( PlayerDeathEvent event ) {
		Player player = (Player) event.getEntity();
		World world = player.getWorld();
		
		// Check world
		if( !plugin.getConfigStore().isEnabledWorld( world ) )
			return;
		
		// If player is killed, prevent it from being targeted
		// TODO Turn into O(1) operation
		for( Entity entity : world.getEntities() ) {
			if( entity instanceof Creature ) {
				Creature creature = (Creature) entity;
				if( creature.getTarget() == player )
					creature.setTarget( null );
			}
		}
	}
}
