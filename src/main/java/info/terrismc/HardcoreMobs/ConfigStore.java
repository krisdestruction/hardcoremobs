package info.terrismc.HardcoreMobs;

import java.util.List;

import org.bukkit.World;

public class ConfigStore {
	private HardcoreMobs plugin;

	public ConfigStore( HardcoreMobs plugin ) {
		this.plugin = plugin;
		
		reloadConfig();
	}
	
	public void reloadConfig() {
		plugin.saveDefaultConfig();
		plugin.reloadConfig();
	}

	public boolean isEnabledWorld(World world) {
		List<String> worldList = plugin.getConfig().getStringList( "worlds" );
		return worldList.contains( "All" ) || worldList.contains( "all" ) || worldList.contains( world.getName() );
	}

}
