package info.terrismc.HardcoreMobs;

import java.util.Comparator;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class DistanceComparator implements Comparator<LivingEntity> {
	private double cX;
	private double cY;
	private double cZ;

	public DistanceComparator( Location loc ) {
		cX = loc.getX();
		cY = loc.getX();
		cZ = loc.getX();
	}
	
	public int compare( LivingEntity p, LivingEntity q ) {
		// Get player locations
		Location pLoc = p.getLocation();
		Location qLoc = q.getLocation();
		
		// Entity p distance
		double pX = cX - pLoc.getX();
		double pY = cY - pLoc.getY();
		double pZ = cZ - pLoc.getZ();

		// Entity q distance
		double qX = cX - qLoc.getX();
		double qY = cY - qLoc.getY();
		double qZ = cZ - qLoc.getZ();
		
		// Return distance
		return (int) ( (pX * pX + pY * pY + pZ + pZ) - (qX * qX + qY * qY + qZ + qZ) );
	}
}